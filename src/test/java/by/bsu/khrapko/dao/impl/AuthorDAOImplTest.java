package by.bsu.khrapko.dao.impl;

import by.bsu.khrapko.dao.AuthorDAO;
import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.domain.Author;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.unitils.UnitilsJUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Alena_Khrapko on 6/29/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dataset/authorDAOTest.xml")
@DatabaseTearDown(value = {"classpath:dataset/authorDAOTest.xml"}, type = DatabaseOperation.DELETE_ALL)

public class AuthorDAOImplTest extends UnitilsJUnit4 {

    @Autowired
    private AuthorDAO authorDAO;

    @Test
    public void testCreate() throws DAOException {
        Author author = new Author(null, "Author 4", null);
        Boolean res = authorDAO.create(author);
        assertTrue(res);
    }

    @Test
    public void testUpdate() throws DAOException{
        Author author = new Author(1L, "Author 1 updated", null);
        Boolean res = authorDAO.update(author);
        assertTrue(res);
        Author author1 = authorDAO.select(1L);
        assertEquals(author, author1);
    }

    @Test
    public void testDelete() throws DAOException {
        Boolean res = authorDAO.delete(3L);
        assertFalse(res);
    }

    @Test
    public void testSelectById() throws DAOException {
        Author author = authorDAO.select(3L);
        assertEquals(author, new Author(3L, "Author 3", null));
    }

    @Test
    public void testSelectAll() throws DAOException {
        List<Author> expected = new ArrayList<Author>();
        expected.add(new Author(1L, "Author 1", null));
        expected.add(new Author(2L, "Author 2", null));
        expected.add(new Author(3L, "Author 3", null));
        List<Author> actual = authorDAO.selectAll();
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    //@Ignore
    @Test
    public void testSelectByNewsId() throws DAOException{
        Author author = new Author(3L, "Author 3", null);
        Author actual = authorDAO.selectByNews(2L);
        assertEquals(author, actual);
    }
}
