package by.bsu.khrapko.dao.impl;

import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.NewsDAO;
import by.bsu.khrapko.domain.News;
import by.bsu.khrapko.domain.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Alena_Khrapko on 6/29/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dataset/newsDAOTest.xml")
@DatabaseTearDown(value = { "classpath:dataset/newsDAOTest.xml" }, type = DatabaseOperation.DELETE_ALL)
public class NewsDAOImplTest extends UnitilsJUnit4 {

    @Autowired
    private NewsDAO newsDAO;

    @Test
    public void testCreate() throws DAOException {
        News news = new News(null, "Title", "Short text", "Full text",
                new Timestamp(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
        Boolean result = newsDAO.create(news);
        assertTrue(result);
    }

    @Test
    public void testUpdate() throws DAOException {
        News news = new News(1L, "Title", "Short text", "Full text",
                new Timestamp(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
        Boolean result = newsDAO.update(news);
        assertTrue(result);
    }

    @Test
    public void testDelete() throws DAOException {
        assertTrue(newsDAO.delete(3L));
    }

    @Test
    public void testSelectById() throws DAOException {
        News expected = new News(1L, "Title 1", "Short text 1", "Full text 1",
                Timestamp.valueOf("2014-09-02 00:00:00"), Date.valueOf("2015-03-08"));
        News actual = newsDAO.select(1L);
        assertEquals(expected, actual);
    }

    @Test
    public void testSelectAll() throws DAOException {
        List<News> expected = new ArrayList<>();
        expected.add(new News(1L, "Title 1", "Short text 1", "Full text 1",
                Timestamp.valueOf("2014-09-02 00:00:00"), Date.valueOf("2015-03-08")));
        expected.add(new News(2L, "Title 2", "Short text 2", "Full text 2",
                Timestamp.valueOf("2014-08-12 00:00:00"), Date.valueOf("2015-04-28")));
        expected.add(new News(3L, "Title 3", "Short text 3", "Full text 3",
                Timestamp.valueOf("2016-06-22 00:00:00"), Date.valueOf("2016-07-08")));
        List<News> actual = newsDAO.selectAll();
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void testCount() throws DAOException {
        assertEquals(3, newsDAO.count());
    }

    @Test
    public void testDeleteNewsAuthor() throws DAOException {
        assertTrue(newsDAO.deleteNewsAuthor(1L));
    }

    @Test
    public void testDeleteNewsTag() throws DAOException {
        assertTrue(newsDAO.deleteNewsTag(1L));
    }

    @Test
    public void testInsertNewsAuthor() throws DAOException {
        assertTrue(newsDAO.insertNewsAuthor(1L, 3L));
    }

    @Test
    public void testInsertNewsTag() throws DAOException {
        assertTrue(newsDAO.insertNewsTag(1L, 3L));
    }

    @Test
    public void testBySearchCriteria() throws DAOException {
        Long authorId = 1L;
        List<Long> tagIdList = new ArrayList<>();
        tagIdList.add(3L);
        List<News> expected = new ArrayList<>();
        expected.add(new News(1L, "Title 1", "Short text 1", "Full text 1",
                Timestamp.valueOf("2014-09-02 00:00:00"), Date.valueOf("2015-03-08")));
        assertArrayEquals(expected.toArray(), newsDAO.selectBySearchCriteria(new SearchCriteria(authorId, null)).toArray());
        assertTrue(newsDAO.selectBySearchCriteria(new SearchCriteria(authorId, tagIdList)).isEmpty());
        tagIdList.add(2L);
        assertArrayEquals(expected.toArray(), newsDAO.selectBySearchCriteria(new SearchCriteria(authorId, tagIdList)).toArray());
        expected.add(new News(2L, "Title 2", "Short text 2", "Full text 2",
                Timestamp.valueOf("2014-08-12 00:00:00"), Date.valueOf("2015-04-28")));
        assertArrayEquals(expected.toArray(), newsDAO.selectBySearchCriteria(new SearchCriteria(null, tagIdList)).toArray());
    }
}
