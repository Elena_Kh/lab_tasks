package by.bsu.khrapko.dao.impl;

import by.bsu.khrapko.dao.CommentDAO;
import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.domain.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Alena_Khrapko on 6/29/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dataset/commentDAOTest.xml")
@DatabaseTearDown(value = { "classpath:dataset/commentDAOTest.xml" }, type = DatabaseOperation.DELETE_ALL)

public class CommentDAOImplTest extends UnitilsJUnit4 {

    @Autowired
    private CommentDAO commentDAO;

    @Test
    public void testCreate() throws DAOException {
        Comment comment = new Comment(null, 2L, "Comment text 4", new Timestamp(System.currentTimeMillis()));
        Boolean actual = commentDAO.create(comment);
        assertTrue(actual);
    }

    @Test
    public void testUpdate() throws DAOException {
        Comment comment = new Comment(1L, 2L, "Comment text 4", new Timestamp(System.currentTimeMillis()));
        Boolean actual = commentDAO.update(comment);
        assertTrue(actual);
    }

    @Test
    public void testDelete() throws DAOException {
        Boolean actual = commentDAO.delete(2L);
        assertTrue(actual);
    }

    @Test
    public void testSelectById() throws DAOException {
        Comment expected = new Comment(1L, 1L, "Comment text 1", Timestamp.valueOf("2015-07-06 00:00:00"));
        Comment actual = commentDAO.select(1L);
        assertEquals(expected, actual);
    }

    @Test
    public void testSelectAll() throws DAOException {
        List<Comment> expected = new ArrayList<>();
        expected.add(new Comment(1L, 1L, "Comment text 1", Timestamp.valueOf("2015-07-06 00:00:00")));
        expected.add(new Comment(2L, 1L, "Comment text 2", Timestamp.valueOf("2016-01-08 00:00:00")));
        expected.add(new Comment(3L, 3L, "Comment text 3", Timestamp.valueOf("2015-11-23 00:00:00")));
        List<Comment> actual = commentDAO.selectAll();
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void testCount() throws DAOException {
        assertEquals(2, commentDAO.count(1L));
    }

    @Test
    public void testSelectAllByNewsId() throws DAOException {
        List<Comment> expected = new ArrayList<>();
        expected.add(new Comment(1L, 1L, "Comment text 1", Timestamp.valueOf("2015-07-06 00:00:00")));
        expected.add(new Comment(2L, 1L, "Comment text 2", Timestamp.valueOf("2016-01-08 00:00:00")));
        List<Comment> actual = commentDAO.selectAll(1L);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void testDeleteByNewsId() throws DAOException {
        Long newsId = 1L;
        Boolean actual = commentDAO.deleteByNewsId(newsId);
        assertTrue(actual);
    }
}
