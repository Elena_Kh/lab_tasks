package by.bsu.khrapko.service.impl;

import by.bsu.khrapko.dao.AuthorDAO;
import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.domain.Author;
import by.bsu.khrapko.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;

/**
 * Created by Alena_Khrapko on 7/4/2016.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceImplTest {

    @Mock
    private AuthorDAO authorDAO;

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave() throws DAOException, ServiceException{
        Author author1 = new Author(null, "Author 1", null);
        Author author2 = new Author(1L, "Author 2", null);
        Mockito.when(authorDAO.create(author1)).thenReturn(true);
        Mockito.when(authorDAO.update(author2)).thenReturn(true);
        Boolean return1 = authorService.save(author1);
        Boolean return2 = authorService.save(author2);
        Mockito.verify(authorDAO, times(1)).create(author1);
        Mockito.verify(authorDAO, times(1)).update(author2);
        Assert.assertTrue(return1);
        Assert.assertTrue(return2);
    }

    @Test
    public void testSelectAll() throws DAOException, ServiceException{
        List<Author> expectedList = new ArrayList<Author>();
        expectedList.add(new Author(1L, "Author 1", null));
        expectedList.add(new Author(2L, "Author 2", null));
        Mockito.when(authorDAO.selectAll()).thenReturn(expectedList);
        List<Author> actualList = authorService.selectAll();
        Mockito.verify(authorDAO, times(1)).selectAll();
        Assert.assertEquals(expectedList, actualList);
    }

    @Test
    public void testSelect() throws DAOException, ServiceException {
        Long authorId = 1L;
        Author expected = new Author(authorId, "Author 1", null);
        Mockito.when(authorDAO.select(authorId)).thenReturn(expected);
        Author actual = authorService.select(authorId);
        Mockito.verify(authorDAO, times(1)).select(authorId);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testSelectByNewsId() throws DAOException, ServiceException {
        Long newsId = 1L;
        Author expected = new Author(20L, "Author 20", null);
        Mockito.when(authorDAO.selectByNews(newsId)).thenReturn(expected);
        Author actual = authorService.selectByNews(newsId);
        Mockito.verify(authorDAO, times(1)).selectByNews(newsId);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDelete() throws DAOException, ServiceException{
        Mockito.when(authorDAO.delete(anyLong())).thenReturn(Boolean.FALSE);
        Boolean actual = authorService.delete(1L);
        Mockito.verify(authorDAO, times(1)).delete(anyLong());
        Assert.assertFalse(actual);
    }
}
