package by.bsu.khrapko.service.impl;

import by.bsu.khrapko.dao.CommentDAO;
import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.domain.Comment;
import by.bsu.khrapko.service.CommentService;
import by.bsu.khrapko.service.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Alena_Khrapko on 7/4/2016.
 */
public class CommentServiceImplTest {

    @Mock
    private CommentDAO commentDAO;

    @InjectMocks
    private CommentServiceImpl commentService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCount() throws DAOException, ServiceException {
        Long newsId = 1L;
        when(commentDAO.count(newsId)).thenReturn(5);
        Integer actual = commentService.count(newsId);
        verify(commentDAO, times(1)).count(newsId);
        assertEquals(new Integer(5), actual);
    }

    @Test
    public void testSave() throws ServiceException, DAOException {
        Comment comment = new Comment(1L, 1L, "Comment text", new Timestamp(System.currentTimeMillis()));
        Comment comment1 = new Comment(null, 2L, "Comment text", new Timestamp(System.currentTimeMillis()));
        commentService.save(comment);
        verify(commentDAO, times(1)).update(comment);
        commentService.save(comment1);
        verify(commentDAO, times(1)).create(comment1);
    }

    @Test
    public void testSelectAll() throws ServiceException, DAOException{
        List<Comment> expectedList = new ArrayList<Comment>();
        expectedList.add(new Comment(1L, 2L, "Comment 1", new Timestamp(System.currentTimeMillis())));
        expectedList.add(new Comment(2L, 3L, "Comment 2", new Timestamp(System.currentTimeMillis())));
        when(commentDAO.selectAll()).thenReturn(expectedList);
        List<Comment> actualList = commentService.selectAll();
        verify(commentDAO, times(1)).selectAll();
        assertEquals(expectedList, actualList);
    }

    @Test
    public void testSelectAllByNewsId() throws ServiceException, DAOException{
        Long newsId = 3L;
        List<Comment> expectedList = new ArrayList<Comment>();
        expectedList.add(new Comment(1L, 2L, "Comment 1", new Timestamp(System.currentTimeMillis())));
        expectedList.add(new Comment(2L, 3L, "Comment 2", new Timestamp(System.currentTimeMillis())));
        when(commentDAO.selectAll(newsId)).thenReturn(expectedList);
        List<Comment> actualList = commentService.selectAll(newsId);
        verify(commentDAO, times(1)).selectAll(newsId);
        assertEquals(expectedList, actualList);
    }

    @Test
    public void testSelect() throws DAOException, ServiceException{
        Long id = 1L;
        Comment expected = new Comment(1L, 2L, "Comment 1", new Timestamp(System.currentTimeMillis()));
        when(commentDAO.select(id)).thenReturn(expected);
        Comment actual = commentService.select(id);
        verify(commentDAO, times(1)).select(id);
        assertEquals(expected, actual);
    }

    @Test
    public void testDelete() throws DAOException, ServiceException{
        Long id = 1L;
        when(commentDAO.delete(id)).thenReturn(Boolean.TRUE);
        Boolean actual = commentService.delete(id);
        assertTrue(actual);
    }
}
