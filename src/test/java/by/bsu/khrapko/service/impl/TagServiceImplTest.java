package by.bsu.khrapko.service.impl;

import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.TagDAO;
import by.bsu.khrapko.domain.Tag;
import by.bsu.khrapko.service.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Alena_Khrapko on 7/4/2016.
 */
public class TagServiceImplTest {

    @Mock
    private TagDAO tagDAO;

    @InjectMocks
    private TagServiceImpl tagService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSelectByNewsId() throws DAOException, ServiceException {
        Long newsId = 5L;
        List<Tag> expectedList = new ArrayList<Tag>();
        expectedList.add(new Tag(1L, "Tag 1"));
        expectedList.add(new Tag(2L, "Tag 2"));
        when(tagDAO.selectByNewsId(newsId)).thenReturn(expectedList);
        List<Tag> actualList = tagService.selectByNewsId(newsId);
        assertEquals(expectedList, actualList);
    }

    @Test
    public void testSelectAll() throws ServiceException, DAOException {
        List<Tag> expectedList = new ArrayList<Tag>();
        expectedList.add(new Tag(1L, "Tag 1"));
        expectedList.add(new Tag(2L, "Tag 2"));
        when(tagDAO.selectAll()).thenReturn(expectedList);
        List<Tag> actualList = tagService.selectAll();
        assertEquals(expectedList, actualList);
    }

    @Test
    public void testDeleteNewsTag() throws ServiceException, DAOException{
        Long tagId = 5L;
        when(tagDAO.deleteNewsTag(tagId)).thenReturn(Boolean.TRUE);
        Boolean actual = tagService.deleteNewsTag(tagId);
        assertTrue(actual);
    }

    @Test
    public void testDelete() throws DAOException, ServiceException {
        Long id = 1L;
        when(tagDAO.delete(id)).thenReturn(Boolean.TRUE);
        when(tagDAO.deleteNewsTag(id)).thenReturn(Boolean.TRUE);
        when(tagDAO.deleteNewsTag(2L)).thenReturn(Boolean.FALSE);
        Boolean actual = tagService.delete(2L);
        verify(tagDAO, times(1)).deleteNewsTag(2L);
        verify(tagDAO, times(0)).delete(2L);
        assertFalse(actual);
        actual = tagService.delete(id);
        verify(tagDAO, times(1)).deleteNewsTag(id);
        verify(tagDAO, times(1)).delete(id);
        assertTrue(actual);
    }

    @Test
    public void testSave() throws DAOException, ServiceException{
        Tag tag1 = new Tag(1L, "Tag 1");
        Tag tag2 = new Tag(null, "Tag 2");
        when(tagDAO.create(tag2)).thenReturn(Boolean.TRUE);
        when(tagDAO.update(tag1)).thenReturn(Boolean.TRUE);
        Boolean actual = tagService.save(tag1);
        verify(tagDAO, times(1)).update(tag1);
        assertTrue(actual);
        actual = tagService.save(tag2);
        verify(tagDAO, times(1)).create(tag2);
        assertTrue(actual);
    }

}
