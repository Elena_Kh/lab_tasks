package by.bsu.khrapko.service.impl;

import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.NewsDAO;
import by.bsu.khrapko.service.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Alena_Khrapko on 7/4/2016.
 */
public class NewsServiceImplTest {

    @Mock
    private NewsDAO newsDAO;

    @InjectMocks
    private NewsServiceImpl newsService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDelete() throws DAOException, ServiceException {
        Long id = 1L;
        when(newsDAO.deleteNewsTag(id)).thenReturn(Boolean.TRUE);
        when(newsDAO.deleteNewsAuthor(id)).thenReturn(Boolean.TRUE);
        when(newsDAO.delete(id)).thenReturn(Boolean.TRUE);
        Boolean actual = newsService.delete(id);
        verify(newsDAO, times(1)).deleteNewsTag(id);
        verify(newsDAO, times(1)).deleteNewsAuthor(id);
        verify(newsDAO, times(1)).delete(id);
        assertTrue(actual);
    }
}
