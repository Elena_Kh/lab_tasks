package by.bsu.khrapko.domain;

/**
 * Created by Alena_Khrapko on 6/24/2016.
 */
public class User{
    private Long id;
    private String userName;
    private String login;
    private String password;

    public User(Long id, String userName, String login, String password) {
        this.id = id;
        this.userName = userName;
        this.login = login;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
