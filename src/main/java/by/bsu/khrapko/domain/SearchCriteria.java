package by.bsu.khrapko.domain;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
public class SearchCriteria {
    private Long authorId;
    private List<Long> tagIdList;

    public SearchCriteria(Long authorId, List<Long> tagIdList) {
        this.authorId = authorId;
        this.tagIdList = tagIdList;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public List<Long> getTagIdList() {
        return tagIdList;
    }
}
