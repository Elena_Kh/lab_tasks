package by.bsu.khrapko.domain;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
public class Role{
    private Long userId;
    private String roleName;

    public Role(Long userId, String roleName) {
        this.userId = userId;
        this.roleName = roleName;
    }

    public Role(String roleName) {
        this.roleName = roleName;
    }

    public Long getUserId() {
        return userId;
    }

    public String getRoleName() {
        return roleName;
    }
}
