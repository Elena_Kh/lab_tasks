package by.bsu.khrapko.domain;

/**
 * Created by Alena_Khrapko on 6/23/2016.
 */
public class Tag{
    private Long id;
    private String tagName;

    public Tag(Long id, String tagName) {
        this.id = id;
        this.tagName = tagName;
    }

    public Long getId() {
        return id;
    }

    public String getTagName() {
        return tagName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;

        Tag tag = (Tag) o;

        if (!getId().equals(tag.getId())) return false;
        return getTagName().equals(tag.getTagName());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getTagName().hashCode();
        return result;
    }
}
