package by.bsu.khrapko.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Alena_Khrapko on 6/21/2016.
 */
public class Comment{
    private Long id;
    private Long newsId;
    private String commentText;
    private Timestamp creationDate;

    public Comment(Long id, Long newsId, String commentText, Timestamp creationDate) {
        this.id = id;
        this.newsId = newsId;
        this.commentText = commentText;
        this.creationDate = creationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;

        Comment comment = (Comment) o;

        if (!getId().equals(comment.getId())) return false;
        if (!getNewsId().equals(comment.getNewsId())) return false;
        if (!getCommentText().equals(comment.getCommentText())) return false;
        return getCreationDate().equals(comment.getCreationDate());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getNewsId().hashCode();
        result = 31 * result + getCommentText().hashCode();
        result = 31 * result + getCreationDate().hashCode();
        return result;
    }
}
