package by.bsu.khrapko.dao;

import by.bsu.khrapko.domain.News;
import by.bsu.khrapko.domain.SearchCriteria;

import java.util.List;

public interface NewsDAO extends DAO<News> {

    int count() throws DAOException;
    boolean deleteNewsAuthor(Long id) throws DAOException;
    boolean deleteNewsTag (Long id) throws DAOException;
    boolean insertNewsAuthor(Long newsId, Long authorId) throws DAOException;
    boolean insertNewsTag (Long newsId, Long tagId) throws DAOException;
    List<News> selectBySearchCriteria (SearchCriteria searchCriteria) throws DAOException;
}
