package by.bsu.khrapko.dao;

import by.bsu.khrapko.domain.Comment;

import java.util.List;

public interface CommentDAO extends DAO<Comment> {

    int count(Long newsId) throws DAOException;
    List<Comment> selectAll(Long newsId) throws DAOException;
    boolean deleteByNewsId(Long newsId) throws DAOException;
}
