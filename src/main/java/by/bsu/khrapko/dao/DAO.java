package by.bsu.khrapko.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public interface DAO<T> {

    List<T> selectAll() throws DAOException;
    T select(Long id) throws DAOException;
    boolean delete(Long id) throws DAOException;
    boolean create(T entity) throws DAOException;
    boolean update(T entity) throws DAOException;

    default void setStatment(PreparedStatement pr, Object... args) throws DAOException {
        try {
            for (int i=0; i<args.length; i++) {
                if (args[i] == null){
                    //String str = new String(Timestamp);
                    pr.setString(i+1, null);
                }else if (args[i] instanceof Timestamp) {
                    pr.setTimestamp(i + 1, (Timestamp) args[i]);
                }else if (args[i] instanceof Date){
                    pr.setDate(i+1, (Date) args[i]);
                }else {
                    pr.setString(i+1, args[i].toString());
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
