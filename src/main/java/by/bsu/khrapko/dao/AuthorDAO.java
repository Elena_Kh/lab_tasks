package by.bsu.khrapko.dao;

import by.bsu.khrapko.domain.Author;

public interface AuthorDAO extends DAO<Author> {
    Author selectByNews (Long newsId) throws DAOException;
}
