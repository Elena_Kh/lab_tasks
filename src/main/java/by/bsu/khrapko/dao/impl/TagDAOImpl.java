package by.bsu.khrapko.dao.impl;

import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.TagDAO;
import by.bsu.khrapko.domain.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TagDAOImpl implements TagDAO{

    private static final String SQL_SELECT_ALL_TAG = "select tag_id, tag_name from tag order by tag_id asc";
    private static final String SQL_SELECT_TAG = "select tag_name from tag where tag_id = ?";
    private static final String SQL_INSERT_TAG = "insert into tag (tag_id, tag_name) values (tag_seq.nextval, ?)";
    private static final String SQL_UPDATE_TAG = "update tag set tag_name = ? where tag_id = ?";
    private static final String SQL_DELETE_TAG = "delete from tag where tag_id = ?";
    private static final String SQL_DELETE_NEWS_TAG = "delete from news_tag where tag_id = ?";
    private static final String SQL_SELECT_BY_NEWS_ID = "select tag.tag_id, tag.tag_name from tag " +
            "inner join news_tag on tag.tag_id = news_tag.tag_id where news_id = ? order by tag_id asc";

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Tag> selectAll() throws DAOException {
        List<Tag> tagList = new ArrayList<>();
        Tag tag = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_ALL_TAG);
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                tag = new Tag(rs.getLong("tag_id"), rs.getString("tag_name"));
                tagList.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tagList;
    }

    @Override
    public Tag select(Long id) throws DAOException {
        Tag tag = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_TAG);
            pr.setString(1, Long.toString(id));
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                tag = new Tag(id, rs.getString("tag_name"));
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tag;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_DELETE_TAG);
            pr.setString(1, Long.toString(id));
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean create(Tag entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_INSERT_TAG);
            pr.setString(1, entity.getTagName());
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean update(Tag entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_UPDATE_TAG);
            setStatment(pr, entity.getTagName(), entity.getId());
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public List<Tag> selectByNewsId(Long newsId) throws DAOException {
        List<Tag> tagList = new ArrayList<>();
        Tag tag = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_BY_NEWS_ID);
            pr.setString(1, Long.toString(newsId));
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                tag = new Tag(rs.getLong("tag_id"), rs.getString("tag_name"));
                tagList.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tagList;
    }

    @Override
    public boolean deleteNewsTag(Long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_DELETE_NEWS_TAG);
            pr.setString(1, Long.toString(tagId));
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }
}
