package by.bsu.khrapko.dao.impl;

import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.NewsDAO;
import by.bsu.khrapko.domain.News;
import by.bsu.khrapko.util.SearchBuilder;
import by.bsu.khrapko.domain.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class NewsDAOImpl implements NewsDAO {

    private static final String SQL_SELECT_ALL = "select news_id, title, short_text, full_text, creation_date, " +
            "modification_date from news order by news_id asc";
    private static final String SQL_SELECT_BY_ID = "select news_id, title, short_text, full_text, creation_date, " +
            "modification_date from news where news_id = ?";
    private static final String SQL_INSERT_NEWS = "insert into news (news_id, title, short_text, full_text, creation_date, " +
            "modification_date) values (news_seq.nextval, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_NEWS = "update news set title = ?, short_text = ?, full_text = ?, creation_date = ?, " +
            "modification_date = ? where news_id = ?";
    private static final String SQL_DELETE_NEWS = "delete from news where news_id = ?";
    private static final String SQL_DELETE_NEWS_AUTHOR = "delete from news_author where news_id = ?";
    private static final String SQL_DELETE_NEWS_TAG = "delete from news_tag where news_id = ?";
    private static final String SQL_COUNT_NEWS = "select count(*) as quantity from news";
    private static final String SQL_INSERT_NEWS_AUTHOR = "insert into news_author (news_id, author_id) values (?, ?)";
    private static final String SQL_INSERT_NEWS_TAG = "insert into news_tag (news_id, tag_id) values (?, ?)";

    @Autowired
    private DataSource dataSource;

    public List<News> selectAll() throws DAOException {
        List<News> newsList = new ArrayList<News>();
        News news = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_ALL);
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                news = new News(rs.getLong("news_id"), rs.getString("title"), rs.getString("short_text"),
                        rs.getString("full_text"), rs.getTimestamp("creation_date"), rs.getDate("modification_date"));
                newsList.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return newsList;
    }

    public News select(Long id) throws DAOException {
        News news = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_BY_ID);
            pr.setString(1, Long.toString(id));
            ResultSet rs = pr.executeQuery();
            rs.next();
            news = new News(rs.getLong("news_id"), rs.getString("title"), rs.getString("short_text"),
                    rs.getString("full_text"), rs.getTimestamp("creation_date"), rs.getDate("modification_date"));
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return news;
    }

    public boolean delete(Long id) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_DELETE_NEWS);
            pr.setString(1, Long.toString(id));
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    public boolean create(News entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_INSERT_NEWS);
            setStatment(pr, entity.getTitle(), entity.getShortText(), entity.getFullText(),
                    entity.getCreationDate(), entity.getModificationDate());
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    public boolean update(News entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_UPDATE_NEWS);
            setStatment(pr, entity.getTitle(), entity.getShortText(), entity.getFullText(),
                    entity.getCreationDate(), entity.getModificationDate(), entity.getId());
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    public int count() throws DAOException {
        int num;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_COUNT_NEWS);
            ResultSet resultSet = pr.executeQuery();
            resultSet.next();
            num = resultSet.getInt("quantity");
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return num;
    }

    @Override
    public boolean deleteNewsAuthor(Long id) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR);
            pr.setString(1, Long.toString(id));
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean deleteNewsTag(Long id) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_DELETE_NEWS_TAG);
            pr.setString(1, Long.toString(id));
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean insertNewsAuthor(Long newsId, Long authorId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_INSERT_NEWS_AUTHOR);
            setStatment(pr, Long.toString(newsId), Long.toString(authorId));
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean insertNewsTag(Long newsId, Long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_INSERT_NEWS_TAG);
            setStatment(pr, Long.toString(newsId), Long.toString(tagId));
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public List<News> selectBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {
        List<News> newsList = new ArrayList<News>();
        News news = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SearchBuilder.buildQuery(searchCriteria));
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                news = new News(rs.getLong("news_id"), rs.getString("title"), rs.getString("short_text"),
                        rs.getString("full_text"), rs.getTimestamp("creation_date"), rs.getDate("modification_date"));
                newsList.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return newsList;
    }
}
