package by.bsu.khrapko.dao.impl;

import by.bsu.khrapko.dao.CommentDAO;
import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.domain.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Repository
public class CommentDAOImpl implements CommentDAO {

    private static final String SQL_SELECT_ALL =
            "select comment_id, news_id, comment_text, creation_date from comments order by comment_id asc";
    private static final String SQL_SELECT_ALL_BY_NEWS_ID = "select comment_id, comment_text, creation_date " +
            "from comments where news_id = ? order by comment_id asc";
    private static final String SQL_SELECT_COMMENT =
            "select news_id, comment_text, creation_date from comments where comment_id = ?";
    private static final String SQL_INSERT_COMMENT = "insert into comments (comment_id, news_id, comment_text, " +
            "creation_date) values (comment_seq.nextval, ?, ?, ?)";
    private static final String SQL_DELETE_COMMENT = "delete from comments where comment_id = ?";
    private static final String SQL_DELETE_COMMENT_BY_NEWS_ID = "delete from comments where news_id = ?";
    private static final String SQL_UPDATE_COMMENT = "update comments set news_id = ?, comment_text = ?, creation_date = ? " +
            "where comment_id = ?";
    private static final String SQL_COUNT_COMMENT = "select count(*) as quantity from comments where news_id = ?";

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Comment> selectAll() throws DAOException {
        List<Comment> commentList = new ArrayList<Comment>();
        Comment comment = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_ALL);
           ResultSet rs = pr.executeQuery();
            while (rs.next()){
                comment = new Comment(rs.getLong("comment_id"), rs.getLong("news_id"), rs.getString("comment_text"),
                        rs.getTimestamp("creation_date"));
                commentList.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return commentList;
    }

    @Override
    public Comment select(Long id) throws DAOException {
        Comment comment = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_COMMENT);
            pr.setString(1, Long.toString(id));
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                comment = new Comment(id, rs.getLong("news_id"), rs.getString("comment_text"), rs.getTimestamp("creation_date"));
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return comment;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_DELETE_COMMENT);
            pr.setString(1, Long.toString(id));
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean create(Comment entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_INSERT_COMMENT);
            setStatment(pr, entity.getNewsId(), entity.getCommentText(), entity.getCreationDate());
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean update(Comment entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_UPDATE_COMMENT);
            setStatment(pr, entity.getNewsId(), entity.getCommentText(), entity.getCreationDate(), entity.getId());
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public int count(Long newsId) throws DAOException {
        int num;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_COUNT_COMMENT);
            pr.setString(1, Long.toString(newsId));
            ResultSet resultSet = pr.executeQuery();
            resultSet.next();
            num = resultSet.getInt("quantity");
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return num;
    }

    @Override
    public List<Comment> selectAll(Long newsId) throws DAOException {
        List<Comment> commentList = new ArrayList<Comment>();
        Comment comment = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_ALL_BY_NEWS_ID);
            pr.setString(1, Long.toString(newsId));
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                comment = new Comment(rs.getLong("comment_id"), newsId, rs.getString("comment_text"),
                        rs.getTimestamp("creation_date"));
                commentList.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return commentList;
    }

    @Override
    public boolean deleteByNewsId(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_DELETE_COMMENT_BY_NEWS_ID);
            pr.setString(1, Long.toString(newsId));
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }
}
