package by.bsu.khrapko.dao.impl;

import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.RoleDAO;
import by.bsu.khrapko.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RoleDAOImpl implements RoleDAO {

    @Autowired
    private DataSource dataSource;

    private static final String SQL_SELECT_ALL = "select distinct role_name from roles";
    private static final String SQL_SELECT_ROLE = "select role_name from roles where user_id = ?";
    private static final String SQL_INSERT_ROLE = "insert into roles (user_id, role_name) values (?, ?)";
    private static final String SQL_UPDATE_ROLE = "update roles set role_name = ? where user_id = ?";
    private static final String SQL_DELETE_ROLE = "delete from roles where user_id = ?";

    @Override
    public List<Role> selectAll() throws DAOException {
        List<Role> roleList = new ArrayList<Role>();
        Role role = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_ALL);
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                role = new Role(rs.getString("role_name"));
                roleList.add(role);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return roleList;
    }

    @Override
    public Role select(Long id) throws DAOException {
        Role role = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_ROLE);
            pr.setString(1, Long.toString(id));
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                role = new Role(id, rs.getString("role_name"));
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return role;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_DELETE_ROLE);
            pr.setString(1, Long.toString(id));
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean create(Role entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_INSERT_ROLE);
            pr.setString(1, entity.getRoleName());
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean update(Role entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_UPDATE_ROLE);
            setStatment(pr, entity.getRoleName(), entity.getUserId());
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }
}
