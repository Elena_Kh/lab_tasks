package by.bsu.khrapko.dao.impl;

import by.bsu.khrapko.dao.AuthorDAO;
import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.domain.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AuthorDAOImpl implements AuthorDAO {

    private static final String SQL_SELECT_AUTHOR = "select author_name, expired from author where author_id = ?";
    private static final String SQL_SELECT_ALL_AUTHOR = "select author_id, author_name, expired from author order by author_id asc";
    private static final String SQL_INSERT_AUTHOR = "insert into author (author_id, author_name, expired) values " +
            "(author_seq.nextval, ?, ?)";
    private static final String SQL_UPDATE_AUTHOR = "update author set author_name = ?, expired = ? where author_id = ?";
    private static final String SQL_SELECT_BY_NEWS_ID = "select author.author_id, author.author_name, author.expired " +
            "from author inner join news_author on author.author_id = news_author.author_id where news_id = ?";

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Author> selectAll() throws DAOException {
        List<Author> authorList = new ArrayList<>();
        Author author = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_ALL_AUTHOR);
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                author = new Author(rs.getLong("author_id"), rs.getString("author_name"), rs.getTimestamp("expired"));
                authorList.add(author);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return authorList;
    }

    @Override
    public Author select(Long id) throws DAOException {
        Author author = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_AUTHOR);
            pr.setLong(1, id);
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                author = new Author(id, rs.getString("author_name"), rs.getTimestamp("expired"));
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return author;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        return false;
    }

    @Override
    public boolean create(Author entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_INSERT_AUTHOR);
            setStatment(pr, entity.getAuthorName(), entity.getExpired());
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean update(Author entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_UPDATE_AUTHOR);
            setStatment(pr, entity.getAuthorName(), entity.getExpired(), entity.getId());
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public Author selectByNews(Long newsId) throws DAOException {
        Author author = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_BY_NEWS_ID);
            pr.setLong(1, newsId);
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                author = new Author(rs.getLong("author_id"), rs.getString("author_name"), rs.getTimestamp("expired"));
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return author;
    }

}

