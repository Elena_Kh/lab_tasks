package by.bsu.khrapko.dao;

import by.bsu.khrapko.domain.Tag;

import java.util.List;

public interface TagDAO extends DAO<Tag> {

    List<Tag> selectByNewsId(Long newsId) throws DAOException;
    boolean deleteNewsTag(Long tagId) throws DAOException;
}
