package by.bsu.khrapko.dao;

import by.bsu.khrapko.domain.Role;

public interface RoleDAO extends DAO<Role> {
}
