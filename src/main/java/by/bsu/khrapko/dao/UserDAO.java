package by.bsu.khrapko.dao;

import by.bsu.khrapko.domain.User;

public interface UserDAO extends DAO<User> {
}
