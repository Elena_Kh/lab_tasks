package by.bsu.khrapko.util;

import by.bsu.khrapko.domain.SearchCriteria;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
public class SearchBuilder {

    private static String str1 = "select distinct news.news_id, title, short_text, full_text, creation_date, modification_date from news ";
    private static String str2 = "inner join news_author on news.news_id = news_author.news_id ";
    private static String str3 = "inner join news_tag on news.news_id = news_tag.news_id ";
    private static String str4 = "where ";
    private static String str5 = "author_id = ";
    private static String str6 = " and ";
    private static String str7 = "tag_id in (";
    private static String str8 = ") ";
    private static String str9 = "order by news.news_id asc";

    public static String buildQuery(SearchCriteria searchCriteria){
        StringBuilder stringBuilder = new StringBuilder(str1);
        if (searchCriteria.getAuthorId() != null){
            stringBuilder.append(str2);
        }
        List<Long> currentList = notNullList(searchCriteria.getTagIdList());
        if (currentList != null && !currentList.isEmpty()){
            stringBuilder.append(str3);
        }
        stringBuilder.append(str4);
        if (searchCriteria.getAuthorId() != null){
            stringBuilder.append(str5);
            stringBuilder.append(searchCriteria.getAuthorId().toString());
        }
        if (searchCriteria.getAuthorId() != null && currentList != null && !currentList.isEmpty()){
            stringBuilder.append(str6);
        }
        if (currentList != null && !currentList.isEmpty()){
            stringBuilder.append(str7);
            for (int i=0; i<currentList.size()-1; i++){
                stringBuilder.append(currentList.get(i).toString() + ", ");
            }
            stringBuilder.append(currentList.get(currentList.size()-1) + str8);
        }
        stringBuilder.append(str9);
        return stringBuilder.toString();
    }

    private static List<Long> notNullList(List<Long> tags){
        if (tags == null || tags.isEmpty()){
            return null;
        }
        List<Long> res = new ArrayList<>();
        for (Long tagId : tags){
            if (tagId != null){
                res.add(tagId);
            }
        }
        return res;
    }
}
