package by.bsu.khrapko.service.impl;

import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.NewsDAO;
import by.bsu.khrapko.domain.News;
import by.bsu.khrapko.domain.SearchCriteria;
import by.bsu.khrapko.service.NewsService;
import by.bsu.khrapko.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
@Service
public class NewsServiceImpl implements NewsService {

    private static Logger logger = LogManager.getLogger(NewsServiceImpl.class);

    @Autowired
    private NewsDAO newsDAO;

    @Override
    public int count() throws ServiceException {
        try {
            return newsDAO.count();
        } catch (DAOException e) {
            logger.error("NewsService count: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteNewsAuthor(Long id) throws ServiceException {
        try {
            return newsDAO.deleteNewsAuthor(id);
        } catch (DAOException e) {
            logger.error("NewsService deleteNewsAuthor: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteNewsTag(Long id) throws ServiceException {
        try {
            return newsDAO.deleteNewsTag(id);
        } catch (DAOException e) {
            logger.error("NewsService deleteNewsTag: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean insertNewsAuthor(Long newsId, Long authorId) throws ServiceException {
        try {
            return newsDAO.insertNewsAuthor(newsId, authorId);
        } catch (DAOException e) {
            logger.error("NewsService insertNewsAuthor: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean insertNewsTag(Long newsId, Long tagId) throws ServiceException {
        try {
            return newsDAO.insertNewsTag(newsId, tagId);
        } catch (DAOException e) {
            logger.error("NewsService insertNewsTag: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> selectBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.selectBySearchCriteria(searchCriteria);
        } catch (DAOException e) {
            logger.error("NewsService selectBySearchCriteria: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> selectAll() throws ServiceException {
        try {
            return newsDAO.selectAll();
        } catch (DAOException e) {
            logger.error("NewsService selectAll: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public News select(Long id) throws ServiceException {
        try {
            return newsDAO.select(id);
        } catch (DAOException e) {
            logger.error("NewsService select: " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * If entity has an Id, then this entity already exists and need to be updated.
     * Otherwise the entity need to be created.
     * @param entity
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean save(News entity) throws ServiceException {
        try{
            if (entity.getId() != null){
                return newsDAO.update(entity);
            }
            return newsDAO.create(entity);
        } catch (DAOException e){
            logger.error("NewsService save: " + e);
            throw new ServiceException(e);
        }

    }

    /**
     * First all rows that involve newsId from table news_tag and news_author are deleted,
     * and then the row that involves newsId from table news is deleted
     * @param id
     * @return
     * @throws ServiceException
     */
    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public boolean delete(Long id) throws ServiceException {
        try {
            if (deleteNewsTag(id) && deleteNewsAuthor(id)){
                return newsDAO.delete(id);
            }
        } catch (DAOException e) {
            logger.error("NewsService delete: " + e);
            throw new ServiceException(e);
        }
        return false;
    }
}
