package by.bsu.khrapko.service.impl;

import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.RoleDAO;
import by.bsu.khrapko.domain.Role;
import by.bsu.khrapko.service.RoleService;
import by.bsu.khrapko.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
@Service
public class RoleServiceImpl implements RoleService {

    private static Logger logger = LogManager.getLogger(RoleServiceImpl.class);

    @Autowired
    private RoleDAO roleDAO;


    public List<Role> selectAll() throws ServiceException {
        try {
            return roleDAO.selectAll();
        } catch (DAOException e) {
            logger.error("RoleService selectAll: " + e);
            throw new ServiceException(e);
        }
    }


    public Role select(Long id) throws ServiceException {
        try {
            return roleDAO.select(id);
        } catch (DAOException e) {
            logger.error("RoleService select: " + e);
            throw new ServiceException(e);
        }
    }


    public boolean create(Role entity) throws ServiceException {
        try {
            return roleDAO.create(entity);
        } catch (DAOException e) {
            logger.error("RoleService create: " + e);
            throw new ServiceException(e);
        }
    }


    public boolean update(Role entity) throws ServiceException {
        try {
            return roleDAO.update(entity);
        } catch (DAOException e) {
            logger.error("RoleService update: " + e);
            throw new ServiceException(e);
        }
    }


    public boolean delete(Long id) throws ServiceException {
        try {
            return roleDAO.delete(id);
        } catch (DAOException e) {
            logger.error("RoleService delete: " + e);
            throw new ServiceException(e);
        }
    }
}
