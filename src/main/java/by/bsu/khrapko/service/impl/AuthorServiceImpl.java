package by.bsu.khrapko.service.impl;

import by.bsu.khrapko.dao.AuthorDAO;
import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.domain.Author;
import by.bsu.khrapko.service.AuthorService;
import by.bsu.khrapko.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
@Service
public class AuthorServiceImpl implements AuthorService {

    private static Logger logger = LogManager.getLogger(AuthorServiceImpl.class);

    @Autowired
    private AuthorDAO authorDAO;

    @Override
    public List<Author> selectAll() throws ServiceException {
        try {
            return authorDAO.selectAll();
        } catch (DAOException e) {
            logger.error("AuthorService selectAll: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Author select(Long id) throws ServiceException {
        try {
            return authorDAO.select(id);
        } catch (DAOException e) {
            logger.error("AuthorService select: " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * If entity has an Id, then this entity already exists and need to be updated.
     * Otherwise the entity need to be created.
     * @param entity
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean save(Author entity) throws ServiceException {
        try{
            if (entity.getId() != null){
                return authorDAO.update(entity);
            }
            return authorDAO.create(entity);
        } catch (DAOException e){
            logger.error("AuthorService save: " + e);
            throw new ServiceException(e);
        }

    }

    @Override
    public Author selectByNews(Long newsId) throws ServiceException {
        try {
            return authorDAO.selectByNews(newsId);
        } catch (DAOException e) {
            logger.error("AuthorService selectByNews: " + e);
            throw new ServiceException(e);
        }
    }


    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            return authorDAO.delete(id);
        } catch (DAOException e) {
            logger.error("AuthorService delete: " + e);
            throw new ServiceException(e);
        }
    }
}
