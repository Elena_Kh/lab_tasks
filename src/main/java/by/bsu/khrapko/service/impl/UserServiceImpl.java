package by.bsu.khrapko.service.impl;

import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.UserDAO;
import by.bsu.khrapko.domain.User;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
@Service
public class UserServiceImpl implements UserService {

    private static Logger logger = LogManager.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDAO userDAO;

    @Override
    public List<User> selectAll() throws ServiceException {
        try {
            return userDAO.selectAll();
        } catch (DAOException e) {
            logger.error("UserService selectAll: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public User select(Long id) throws ServiceException {
        try {
            return userDAO.select(id);
        } catch (DAOException e) {
            logger.error("UserService select: " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * If entity has an Id, then this entity already exists and need to be updated.
     * Otherwise the entity need to be created.
     * @param entity
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean save(User entity) throws ServiceException {
        try{
            if (entity.getId() != null){
                return userDAO.update(entity);
            }
            return userDAO.create(entity);
        } catch (DAOException e){
            logger.error("UserService save: " + e);
            throw new ServiceException(e);
        }

    }

    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            return userDAO.delete(id);
        } catch (DAOException e) {
            logger.error("UserService delete: " + e);
            throw new ServiceException(e);
        }
    }
}
