package by.bsu.khrapko.service.impl;

import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.dao.TagDAO;
import by.bsu.khrapko.domain.Tag;
import by.bsu.khrapko.service.ServiceException;
import by.bsu.khrapko.service.TagService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
@Service
public class TagServiceImpl implements TagService {

    private static Logger logger = LogManager.getLogger(TagServiceImpl.class);

    @Autowired
    private TagDAO tagDAO;

    @Override
    public List<Tag> selectByNewsId(Long newsId) throws ServiceException {
        try {
            return tagDAO.selectByNewsId(newsId);
        } catch (DAOException e) {
            logger.error("TagService selectByNewsId: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteNewsTag(Long tagId) throws ServiceException {
        try {
            return tagDAO.deleteNewsTag(tagId);
        } catch (DAOException e) {
            logger.error("TagService deleteNewsTag: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> selectAll() throws ServiceException {
        try {
            return tagDAO.selectAll();
        } catch (DAOException e) {
            logger.error("TagService selectAll: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Tag select(Long id) throws ServiceException {
        try {
            return tagDAO.select(id);
        } catch (DAOException e) {
            logger.error("TagService select: " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * If entity has an Id, then this entity already exists and need to be updated.
     * Otherwise the entity need to be created.
     * @param entity
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean save(Tag entity) throws ServiceException {
        try{
            if (entity.getId() != null){
                return tagDAO.update(entity);
            }
            return tagDAO.create(entity);
        } catch (DAOException e){
            logger.error("TagService save: " + e);
            throw new ServiceException(e);
        }

    }

    /**
     * First all rows that involve tagId from table news_tag are deleted,
     * and then the row that involves tagId from table tag is deleted
     * @param id
     * @return
     * @throws ServiceException
     */
    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public boolean delete(Long id) throws ServiceException {
        try {
            if (deleteNewsTag(id)) {
                return tagDAO.delete(id);
            }
        } catch (DAOException e) {
            logger.error("TagService delete: " + e);
            throw new ServiceException(e);
        }
        return false;
    }
}
