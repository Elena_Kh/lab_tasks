package by.bsu.khrapko.service.impl;

import by.bsu.khrapko.dao.*;
import by.bsu.khrapko.domain.Author;
import by.bsu.khrapko.domain.News;
import by.bsu.khrapko.domain.Tag;
import by.bsu.khrapko.service.CombinedService;
import by.bsu.khrapko.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
@Service
public class CombinedServiceImpl implements CombinedService {

    private static Logger logger = LogManager.getLogger(CombinedServiceImpl.class);

    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private AuthorDAO authorDAO;

    @Autowired
    private TagDAO tagDAO;

    @Autowired
    private CommentDAO commentDAO;

    /**
     * Create news with its author and tags as one step
     * @param news
     * @param author
     * @param tags
     * @return
     * @throws ServiceException
     */
    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public boolean create(News news, Author author, List<Tag> tags) throws ServiceException {
        boolean flag = false;
        try {
            flag = newsDAO.create(news);
            if (author != null){
                if (authorDAO.select(author.getId()) == null){
                    flag = flag && authorDAO.create(author);
                }
                flag = flag && newsDAO.insertNewsAuthor(news.getId(), author.getId());
            }
            if (tags != null && flag){
                for(Tag tag : tags){
                    if (tagDAO.select(tag.getId()) == null){
                        flag = flag && tagDAO.create(tag);
                    }
                    flag = flag && newsDAO.insertNewsTag(news.getId(), tag.getId());
                }
            }
        } catch (DAOException e) {
            logger.error("CombinedService create: " + e);
            throw new ServiceException(e);
        }
        return flag;
    }

    /**
     * Update news with its author and tags
     * @param news
     * @param author
     * @param tags
     * @return
     * @throws ServiceException
     */
    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public boolean update(News news, Author author, List<Tag> tags) throws ServiceException {
        try {
            boolean flag = newsDAO.update(news);
            if (flag){
                flag = authorDAO.update(author);
            }
            if (flag){
                for (Tag tag : tags){
                    flag = flag && tagDAO.update(tag);
                }
            }
            return flag;
        } catch (DAOException e){
            logger.error("CombinedService update: " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * First delete news, then delete all its comments
     * @param newsId
     * @return
     * @throws ServiceException
     */
    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public boolean delete(Long newsId) throws ServiceException {
        try {
            if (newsDAO.delete(newsId)){
                return commentDAO.deleteByNewsId(newsId);
            }
        } catch (DAOException e) {
            logger.error("CombinedService delete: " + e);
            throw new ServiceException(e);
        }
        return false;
    }

}
