package by.bsu.khrapko.service.impl;

import by.bsu.khrapko.dao.CommentDAO;
import by.bsu.khrapko.dao.DAOException;
import by.bsu.khrapko.domain.Comment;
import by.bsu.khrapko.service.CommentService;
import by.bsu.khrapko.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
@Service
public class CommentServiceImpl implements CommentService {

    private static Logger logger = LogManager.getLogger(CommentServiceImpl.class);

    @Autowired
    private CommentDAO commentDAO;

    /**
     * Counts the number of comments of the particular news
     * @param newsId
     * @return
     * @throws ServiceException
     */
    @Override
    public int count(Long newsId) throws ServiceException {
        try {
            return commentDAO.count(newsId);
        } catch (DAOException e) {
            logger.error("CommentService count:" + e);
            throw new ServiceException(e);
        }
    }

    /**
     * Select all comments that belong to particular news
     * @param newsId
     * @return
     * @throws ServiceException
     */
    @Override
    public List<Comment> selectAll(Long newsId) throws ServiceException {
        try {
            return commentDAO.selectAll(newsId);
        } catch (DAOException e) {
            logger.error("CommentService selectAll by NewsId:" + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Comment> selectAll() throws ServiceException {
        try {
            return commentDAO.selectAll();
        } catch (DAOException e) {
            logger.error("CommentService selectAll:" + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Comment select(Long id) throws ServiceException {
        try {
            return commentDAO.select(id);
        } catch (DAOException e) {
            logger.error("CommentService select by Id:" + e);
            throw new ServiceException(e);
        }
    }

    /**
     * If entity has an Id, then this entity already exists and need to be updated.
     * Otherwise the entity need to be created.
     * @param entity
     * @return
     * @throws ServiceException
     */
    @Override
    public boolean save(Comment entity) throws ServiceException {
        try{
            if (entity.getId() != null){
                return commentDAO.update(entity);
            }
            return commentDAO.create(entity);
        } catch (DAOException e){
            logger.error("CommentService save:" + e);
            throw new ServiceException(e);
        }

    }

    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            return commentDAO.delete(id);
        } catch (DAOException e) {
            logger.error("CommentService delete:" + e);
            throw new ServiceException(e);
        }
    }
}
