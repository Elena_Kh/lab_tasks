package by.bsu.khrapko.service;

import by.bsu.khrapko.domain.Tag;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
public interface TagService extends Service<Tag> {
    List<Tag> selectByNewsId(Long newsId) throws ServiceException;
    boolean deleteNewsTag(Long tagId) throws ServiceException;
}
