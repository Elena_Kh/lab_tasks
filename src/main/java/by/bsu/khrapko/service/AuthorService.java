package by.bsu.khrapko.service;

import by.bsu.khrapko.domain.Author;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
public interface AuthorService extends Service<Author>{

    Author selectByNews(Long newsId) throws ServiceException;
}
