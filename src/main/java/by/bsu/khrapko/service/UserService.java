package by.bsu.khrapko.service;

import by.bsu.khrapko.domain.User;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
public interface UserService extends Service<User> {
}
