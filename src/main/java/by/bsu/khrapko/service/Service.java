package by.bsu.khrapko.service;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
public interface Service<T> {
    List<T> selectAll() throws ServiceException;
    T select(Long id) throws ServiceException;
    boolean save(T entity) throws ServiceException;
    boolean delete(Long id) throws ServiceException;
}
