package by.bsu.khrapko.service;

import by.bsu.khrapko.domain.News;
import by.bsu.khrapko.domain.SearchCriteria;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
public interface NewsService extends Service<News>{

    int count() throws ServiceException;
    boolean deleteNewsAuthor(Long id) throws ServiceException;
    boolean deleteNewsTag (Long id) throws ServiceException;
    boolean insertNewsAuthor(Long newsId, Long authorId) throws ServiceException;
    boolean insertNewsTag (Long newsId, Long tagId) throws ServiceException;
    List<News> selectBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;
}
