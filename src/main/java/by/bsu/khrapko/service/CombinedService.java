package by.bsu.khrapko.service;

import by.bsu.khrapko.dao.NewsDAO;
import by.bsu.khrapko.domain.Author;
import by.bsu.khrapko.domain.News;
import by.bsu.khrapko.domain.Tag;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
public interface CombinedService {

    boolean create (News news, Author author, List<Tag> tags) throws ServiceException;
    boolean update (News news, Author author, List<Tag> tags) throws ServiceException;
    boolean delete (Long newsId) throws ServiceException;
}
