drop table roles;
drop table users;
drop table news_tag;
drop table tag;
drop table comments;
drop table news_author;
drop table author;
drop table news;

create table news 
(
news_id number(20) primary key not null,
title nvarchar2(30) not null,
short_text nvarchar2(100) not null,
full_text nvarchar2(2000) not null,
creation_date timestamp not null,
modification_date date not null
);
commit;

create table comments
(
comment_id number(20) primary key not null,
news_id number(20) references news(news_id) not null,
comment_text nvarchar2(100) not null,
creation_date timestamp not null
);
commit;

create table author
(
author_id number(20) primary key not null,
author_name nvarchar2(30) not null,
expired timestamp
);
commit;

create table news_author
(
news_id number(20) references news(news_id) not null,
author_id number(20) references author(author_id) not null
);
commit;

create table tag
(
tag_id number(20) primary key not null,
tag_name nvarchar2(30) not null
);
commit;

create table news_tag
(
news_id number(20) references news(news_id) not null,
tag_id number(20) references tag(tag_id) not null
);
commit;

create table users
(
user_id number(20) primary key not null,
user_name nvarchar2(50) not null,
login varchar2(30) not null,
password varchar2(30) not null
);
commit;

create table roles
(
user_id number(20) references users(user_id) not null,
role_name varchar2(50) not null
);
commit;